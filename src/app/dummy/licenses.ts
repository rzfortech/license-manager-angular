export class DummyLicenses {
  defaultLicenses = [
    {
      id: 1,
      clientId: 1,
      k1: '',
      k2: '',
      start: 1526774400,
      end: 1529452800,
      type: 1,
      created: 0,
      modified: 0,
    }
  ];

  licenses = [];

  constructor() {
    if (localStorage.getItem('dummyLicenses')) {
      // Loaded users from localStorage
      const dummyData = localStorage.getItem('dummyLicenses');
      this.licenses = JSON.parse(dummyData);
    } else {
      this.licenses = this.defaultLicenses;
      localStorage.setItem('dummyLicenses', JSON.stringify(this.licenses));
    }
  }

  createLicense(license: any) {
    license.id = this.licenses[this.licenses.length - 1].id + 1;
    this.licenses.push(license);
    localStorage.setItem('dummyLicenses', JSON.stringify(this.licenses));
  }

  updateLicense(license: any, id: number) {
    for (const l of this.licenses) {
      if (l.id === id) {
        const index = this.licenses.indexOf(l);
        this.licenses[l] = license;
        localStorage.setItem('dummyLicenses', JSON.stringify(this.licenses));
      }
    }
  }
}
