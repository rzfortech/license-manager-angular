export class DummyLicensesTypes {
  licenseTypes = [
    {
      id: 1,
      description: 'DEMO',
      periodDays: 30
    },
    {
      id: 2,
      description: 'TRIAL',
      periodDays: 90
    },
    {
      id: 3,
      description: 'FULL',
      periodDays: 365
    }
  ];
}
