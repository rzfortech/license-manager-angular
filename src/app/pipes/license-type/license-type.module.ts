import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LicenseTypePipe } from './license-type.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LicenseTypePipe
  ],
  exports: [
    LicenseTypePipe
  ]
})
export class LicenseTypeModule { }
