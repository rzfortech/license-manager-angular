import { LicenseTypeModule } from './license-type.module';

describe('LicenseTypeModule', () => {
  let licenseTypeModule: LicenseTypeModule;

  beforeEach(() => {
    licenseTypeModule = new LicenseTypeModule();
  });

  it('should create an instance', () => {
    expect(licenseTypeModule).toBeTruthy();
  });
});
