import { TestBed, inject } from '@angular/core/testing';

import { IsSalesmanGuard } from './is-salesman.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('IsSalesmanGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [IsSalesmanGuard]
    });
  });

  it('should be created', inject([IsSalesmanGuard], (service: IsSalesmanGuard) => {
    expect(service).toBeTruthy();
  }));
});
