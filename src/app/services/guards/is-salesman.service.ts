import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {UserService} from '../user.service';

@Injectable({
  providedIn: 'root'
})
export class IsSalesmanGuard implements CanActivate {

  constructor(
    private userService: UserService,
    private router: Router) { }

  canActivate() {
    const isSalesman = this.userService.hasRole('SALES');

    if (isSalesman) {
      return true;
    } else {
      this.router.navigate(['/dashboard']);
    }

    return false;
  }
}
