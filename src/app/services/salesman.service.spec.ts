import { TestBed, inject } from '@angular/core/testing';

import { SalesmanService } from './salesman.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SalesmanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [SalesmanService]
    });
  });

  it('should be created', inject([SalesmanService], (service: SalesmanService) => {
    expect(service).toBeTruthy();
  }));
});
