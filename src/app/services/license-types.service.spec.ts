import { TestBed, inject } from '@angular/core/testing';

import { LicenseTypesService } from './license-types.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LicenseTypesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LicenseTypesService],
      imports: [
        HttpClientTestingModule
      ]
    });
  });

  it('should be created', inject([LicenseTypesService], (service: LicenseTypesService) => {
    expect(service).toBeTruthy();
  }));
});
