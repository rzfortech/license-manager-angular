import { Injectable } from '@angular/core';
import { SelfUnsubscribe } from '../shared/self-unsubscribe';
import { LicenseType } from '../models/license-type.model';
import { Observable, Observer } from 'rxjs';
import { RequestManager } from './request-manager.service';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class LicenseTypesService extends SelfUnsubscribe {

  constructor(
    private requestManager: RequestManager,
    private messageService: MessageService
  ) {
    super();
  }

  createLicenseType(linceseType: LicenseType): Observable<boolean> {
    return new Observable<boolean>((observer: Observer<boolean>) => {
      const subscr = this.requestManager.createLicenseType(linceseType)
        .subscribe(
          (response) => {
            observer.next(true);
          },
          (err) => {
            observer.next(false);
            this.messageService.showMessage(err.error.message, 'danger');
          }
        );

      this.addSubscription(subscr);
    });
  }

  updateLicenseType(licenseType: LicenseType, id: number): Observable<boolean> {
    return new Observable<boolean>((observer: Observer<boolean>) => {
      const subscr = this.requestManager.updateLicenseType(licenseType, id)
        .subscribe(
          (response) => {
            observer.next(true);
          },
          (err) => {
            observer.next(false);
            this.messageService.showMessage(err.error.message, 'danger');
          }
        );
      this.addSubscription(subscr);
    });
  }

  getLicenseTypes(): Observable<Array<LicenseType>> {
    return new Observable<Array<LicenseType>>((observer: Observer<Array<LicenseType>>) => {
      const subscr = this.requestManager.getLicenseTypes()
        .subscribe(
          (response) => {
            const licenseTypes: LicenseType[] = [];
            for (const licenseType of response) {
              licenseTypes.push(new LicenseType(licenseType));
            }

            observer.next(licenseTypes);
          },
          (err) => {
            this.messageService.showMessage(err.error.message, 'danger');
          }
        );

      this.addSubscription(subscr);
    });
  }

  getLicenseType(licenseTypeID: number): Observable<LicenseType> {
    return new Observable<LicenseType>((observer: Observer<LicenseType>) => {
      const subscr = this.requestManager.getLicenseType(licenseTypeID)
        .subscribe(
          (response) => {
            const licenseType = new LicenseType(response);
            observer.next(licenseType);
          },
          (err) => {
            this.messageService.showMessage(err.error.message, 'danger');
          }
        );

      this.addSubscription(subscr);
    });
  }

  deleteLicenseType(id: number): Observable<boolean> {
    return new Observable<boolean>((observer: Observer<boolean>) => {
      const subscr = this.requestManager.deleteLicenseType(id)
        .subscribe(
          (response) => {
            observer.next(true);
          },
          (err) => {
            observer.next(false);
            this.messageService.showMessage(err.error.message, 'danger');
          }
        );

      this.addSubscription(subscr);
    });
  }
}
