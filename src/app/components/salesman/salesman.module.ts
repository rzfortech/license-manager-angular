import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoggedInGuard } from '../../services/guards/logged-in.service';
import { IsAnonymousGuard } from '../../services/guards/is-anonymous.service';
import { IsAdminGuard } from '../../services/guards/is-admin.service';
import { IsSalesmanGuard } from '../../services/guards/is-salesman.service';
import { SalesmanEditComponent } from './salesman-edit/salesman-edit.component';
import { ClientComponent } from '../clients/client/client.component';
import { ClientEditComponent } from '../clients/client-edit/client-edit.component';
import { SalesmanComponent } from './salesman/salesman.component';
import { ModalConfirmationModule } from '../../shared/modal/confirmation/confirmation.module';
import { FormsModule } from '@angular/forms';
import { PasswordStrengthBarModule } from 'ng2-password-strength-bar';
import { CustomFormsModule } from 'ng2-validation';
import {
  MatButtonModule,
  MatInputModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatListModule,
  MatIconModule,
  MatSelectModule,
  MatCardModule,
  MatToolbarModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppLayoutComponent } from '../../shared/layout/app/app.component';

const routes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    children: [
      { path: 'salesmans', component: SalesmanComponent, canActivate: [LoggedInGuard, IsAdminGuard] },
      {
        path: 'salesmans/create', component: SalesmanEditComponent, canActivate: [LoggedInGuard, IsAdminGuard],
        data: { ownContent: false, newContent: true }
      },
      {
        path: 'salesmans/:id/edit', component: SalesmanEditComponent, canActivate: [LoggedInGuard, IsAdminGuard],
        data: { ownContent: false, newContent: false }
      },
      { path: 'salesmans/:salesmanID/clients', component: ClientComponent, canActivate: [LoggedInGuard, IsAdminGuard] },
      {
        path: 'salesmans/:salesmanID/clients/create', component: ClientEditComponent, canActivate: [LoggedInGuard, IsAdminGuard],
        data: { newContent: true, ownClient: false }
      },
      {
        path: 'salesmans/:salesmanID/clients/:id/edit', component: ClientEditComponent, canActivate: [LoggedInGuard, IsAdminGuard],
        data: { newContent: false, ownClient: false }
      },
      {
        path: 'my-account/edit', component: SalesmanEditComponent, canActivate: [LoggedInGuard],
        data: { ownContent: true, newContent: false }
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    ModalConfirmationModule,
    FormsModule,
    PasswordStrengthBarModule,
    CustomFormsModule,
    MatButtonModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    MatCardModule,
    MatToolbarModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    SalesmanComponent,
    SalesmanEditComponent
  ],
  providers: [
    LoggedInGuard,
    IsAnonymousGuard,
    IsAdminGuard,
    IsSalesmanGuard
  ]
})
export class SalesmanModule { }
