import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyLicensesComponent } from './my-licenses.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MyLicensesModule } from './my-licenses.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppLayoutModule } from '../../shared/layout/app/app.module';

describe('MyLicensesComponent', () => {
  let component: MyLicensesComponent;
  let fixture: ComponentFixture<MyLicensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MyLicensesModule,
        NgbModule.forRoot(),
        HttpClientTestingModule,
        BrowserAnimationsModule,
        AppLayoutModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyLicensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
