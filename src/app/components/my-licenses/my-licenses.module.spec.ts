import { MyLicensesModule } from './my-licenses.module';

describe('MyLicensesModule', () => {
  let myLicensesModule: MyLicensesModule;

  beforeEach(() => {
    myLicensesModule = new MyLicensesModule();
  });

  it('should create an instance', () => {
    expect(myLicensesModule).toBeTruthy();
  });
});
