import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../models/user.model';
import { LicenseService } from '../../services/license.service';
import { SelfUnsubscribe } from '../../shared/self-unsubscribe';
import { UserService } from '../../services/user.service';
import {
  MatTableDataSource,
  MatPaginator,
  MatSort
} from '@angular/material';
import { License } from '../../models/license.model';

@Component({
  selector: 'app-my-licenses',
  templateUrl: './my-licenses.component.html',
  styleUrls: ['./my-licenses.component.scss']
})
export class MyLicensesComponent extends SelfUnsubscribe implements OnInit, OnDestroy {

  salesmanEntity: User;
  displayedColumns = ['position', 'k1', 'k2', 'start', 'end', 'type', 'expires', 'client', 'actions'];
  dataSource: MatTableDataSource<License>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private licenseService: LicenseService,
    private userService: UserService,
    private route: ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {
    this.getLicenses();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();

    this.dataSource.filter = filterValue;
  }

  getLicenses() {
    const userService = this.userService.getUser().subscribe((user: User) => {
      this.salesmanEntity = user;
      const licenseSubcr = this.licenseService.getAllLicensesBySalesman(this.salesmanEntity.id).subscribe((data: any[]) => {
        data.map((item, index) => {
          item.position = ++index;
        });

        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
      this.addSubscription(licenseSubcr);
    });

    this.addSubscription(userService);
  }

  deleteLicense(id: number) {
    const deleteSubscr = this.licenseService.deleteLicense(id).subscribe((response: boolean) => {
      if (response) {
        this.getLicenses();
      }
    });
  }

  ngOnDestroy() {
    this.dispose();
  }
}
