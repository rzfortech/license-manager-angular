import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoggedInGuard } from '../../services/guards/logged-in.service';
import { IsAnonymousGuard } from '../../services/guards/is-anonymous.service';
import { IsAdminGuard } from '../../services/guards/is-admin.service';
import { IsSalesmanGuard } from '../../services/guards/is-salesman.service';
import { MyLicensesComponent } from './my-licenses.component';
import { LimitToModule } from '../../pipes/limit-to/limit-to.module';
import { UntilNowModule } from '../../pipes/until-now/until-now.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalConfirmationModule } from '../../shared/modal/confirmation/confirmation.module';
import {
  MatTableModule,
  MatButtonModule,
  MatInputModule,
  MatSortModule,
  MatPaginatorModule,
  MatTooltipModule
} from '@angular/material';
import { AppLayoutComponent } from '../../shared/layout/app/app.component';

const routes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    children: [
      { path: 'my-licenses', component: MyLicensesComponent, canActivate: [LoggedInGuard, IsSalesmanGuard] }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    LimitToModule,
    UntilNowModule,
    NgbModule,
    ModalConfirmationModule,
    MatTableModule,
    MatButtonModule,
    MatInputModule,
    MatSortModule,
    MatPaginatorModule,
    MatTooltipModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    MyLicensesComponent
  ],
  providers: [
    LoggedInGuard,
    IsAnonymousGuard,
    IsAdminGuard,
    IsSalesmanGuard
  ]
})
export class MyLicensesModule { }
