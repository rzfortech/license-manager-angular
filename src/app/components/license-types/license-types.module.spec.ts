import { LicenseTypesModule } from './license-types.module';

describe('LicenseTypesModule', () => {
  let licenseTypesModule: LicenseTypesModule;

  beforeEach(() => {
    licenseTypesModule = new LicenseTypesModule();
  });

  it('should create an instance', () => {
    expect(licenseTypesModule).toBeTruthy();
  });
});
