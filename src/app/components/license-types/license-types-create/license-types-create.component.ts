import { Component, OnInit, OnDestroy } from '@angular/core';
import { LicenseType } from '../../../models/license-type.model';
import { NgForm } from '@angular/forms';
import { LicenseTypesService } from '../../../services/license-types.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SelfUnsubscribe } from '../../../shared/self-unsubscribe';

@Component({
  selector: 'app-license-types-create',
  templateUrl: './license-types-create.component.html',
  styleUrls: ['./license-types-create.component.scss']
})
export class LicenseTypesCreateComponent  extends SelfUnsubscribe implements OnInit, OnDestroy {
  licenseType = new LicenseType({
    id: 0
  });

  constructor(
    private licenseTypesService: LicenseTypesService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    super();
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      const licenseTypeSubscr = this.licenseTypesService.createLicenseType(form.value)
        .subscribe((response) => {
          licenseTypeSubscr.unsubscribe();
          if (response) {
            this.router.navigate(['..'], {relativeTo: this.route});
          }
        });
      this.addSubscription(licenseTypeSubscr);
    }
  }

  ngOnDestroy() {
    this.licenseTypesService.dispose();
    this.dispose();
  }
}
