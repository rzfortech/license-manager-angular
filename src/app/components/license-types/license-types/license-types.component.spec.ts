import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseTypesComponent } from './license-types.component';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatTableModule,
  MatInputModule,
  MatButtonModule,
  MatPaginatorModule
} from '@angular/material';
import { ModalConfirmationModule } from '../../../shared/modal/confirmation/confirmation.module';

describe('LicenseTypesComponent', () => {
  let component: LicenseTypesComponent;
  let fixture: ComponentFixture<LicenseTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseTypesComponent ],
      imports: [
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatInputModule,
        MatButtonModule,
        ModalConfirmationModule,
        MatPaginatorModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
