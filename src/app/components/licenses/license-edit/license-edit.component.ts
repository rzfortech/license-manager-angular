import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelfUnsubscribe } from '../../../shared/self-unsubscribe';
import { Router, ActivatedRoute } from '@angular/router';
import { LicenseService } from '../../../services/license.service';
import { License } from '../../../models/license.model';
import { LicenseType } from '../../../models/license-type.model';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { MatDatepickerInputEvent } from '@angular/material';
import { NgForm } from '@angular/forms';
import { MessageService } from '../../../services/message.service';
import { LicenseTypesService } from '../../../services/license-types.service';

@Component({
  selector: 'app-license-edit',
  templateUrl: './license-edit.component.html',
  styleUrls: ['./license-edit.component.scss']
})
export class LicenseEditComponent extends SelfUnsubscribe implements OnInit, OnDestroy {
  licenseEntity: License;
  licenseTypes: LicenseType[] = [];
  date: {
    begin: Date,
    end: Date
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private licenseService: LicenseService,
    private licenseTypesService: LicenseTypesService,
    private messageService: MessageService
  ) {
    super();
  }

  ngOnInit() {
    const params = this.route.snapshot.params;

    if ('id' in params) {
      this.getLicenseById(+params.id);
    }

    this.getLicenseTypes();
  }

  getLicenseById(id): void {
    const licenseSubscr = this.licenseService.getLicense(id).subscribe((license: License) => {
      this.licenseEntity = license;
      this.licenseEntity.start = new Date(this.licenseEntity.start);
      this.licenseEntity.end = new Date(this.licenseEntity.end);
      this.date = {
        begin: this.licenseEntity.start,
        end: this.licenseEntity.end
      };
      this.getLicensePeriod();
    });
    this.addSubscription(licenseSubscr);
  }

  getLicenseTypes(): void {
    const slsubscr = this.licenseTypesService.getLicenseTypes()
      .subscribe((licenseTypesList: LicenseType[]) => {
        this.licenseTypes = licenseTypesList;
      });

    this.addSubscription(slsubscr);
  }

  onLicenseTypeChange(id) {
    const slsubscr = this.licenseTypesService.getLicenseType(id)
      .subscribe((licenseType: LicenseType) => {

        this.licenseEntity.period = licenseType.periodDays;
        this.licenseEntity.start = new Date();
        this.licenseEntity.type.id = licenseType.id;

        this.updateLicenseExpiration();
      });

    this.addSubscription(slsubscr);
  }

  onDateChange(event: MatDatepickerInputEvent<Date>) {
    this.licenseEntity.start = event.value['begin'];
    this.licenseEntity.end = event.value['end'];

    this.getLicensePeriod();
  }

  onPeriodChange(value) {
    this.licenseEntity.period = +value;
    this.updateLicenseExpiration();
  }

  getLicensePeriod() {
    const startDate = this.licenseEntity.start;
    const endDate = this.licenseEntity.end;
    const timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
    const nrOfDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    this.licenseEntity.period = nrOfDays;
  }

  updateLicenseExpiration() {
    this.licenseEntity.end = new Date(this.licenseEntity.start);
    this.licenseEntity.end.setDate(this.licenseEntity.end.getDate() + this.licenseEntity.period);

    this.updateDate();
  }

  updateDate() {
    this.date = {
      begin: this.licenseEntity.start,
      end: this.licenseEntity.end
    };
  }

  onSubmit(form: NgForm) {
    if (form.valid &&
      this.licenseEntity.start instanceof Date &&
      this.licenseEntity.end instanceof Date) {
        const secretKey = this.licenseService
          .generateSecretKey(
            this.licenseEntity.k1,
            this.licenseEntity.start,
            this.licenseEntity.end
          );

        if (!secretKey) {
          this.messageService.showMessage('Problem with K1', 'danger');
          return false;
        }
        this.licenseEntity.k2 = secretKey;

        const licenseSubscr = this.licenseService.updateLicense(this.licenseEntity, this.licenseEntity.id).subscribe((response) => {
          licenseSubscr.unsubscribe();
          if (response) {
            this.router.navigate(['/clients', this.licenseEntity.clientId['id'], 'licenses']);
          }
        });
        this.addSubscription(licenseSubscr);
    }
  }

  ngOnDestroy() {
    this.dispose();
  }
}
