import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Client } from '../../../models/client.model';
import { ClientService } from '../../../services/client.service';
import { SelfUnsubscribe } from '../../../shared/self-unsubscribe';
import { License } from '../../../models/license.model';
import { LicenseService } from '../../../services/license.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-license',
  templateUrl: './license.component.html',
  styleUrls: ['./license.component.scss']
})
export class LicenseComponent extends SelfUnsubscribe implements OnInit, OnDestroy {

  clientEntity: Client;
  licenses: License[];
  displayedColumns = ['position', 'k1', 'k2', 'start', 'end', 'type', 'expires', 'actions'];
  dataSource: MatTableDataSource<License>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private clientService: ClientService,
    private licenseService: LicenseService,
    private route: ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {
    this.getLicenses();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();

    this.dataSource.filter = filterValue;
  }

  getLicenses() {
    const paramsSubscr = this.route.params.subscribe((params: Params) => {
      const clientId = +params.id;
      const clientSubscr = this.clientService.getClient(clientId).subscribe((client: Client) => {
        this.clientEntity = client;
        const subscr = this.licenseService.getLicenses(this.clientEntity.id).subscribe((licenses: License[]) => {
          licenses.map((item, index) => {
            item.position = ++index;
          });

          this.dataSource = new MatTableDataSource(licenses);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        });
        this.addSubscription(subscr);
      });
      this.addSubscription(clientSubscr);
    });
    this.addSubscription(paramsSubscr);
  }

  deleteLicense(id: number) {
    const deleteSubscr = this.licenseService.deleteLicense(id).subscribe((response: boolean) => {
      if (response) {
        this.getLicenses();
      }
    });
  }

  ngOnDestroy() {
    this.dispose();
  }
}
