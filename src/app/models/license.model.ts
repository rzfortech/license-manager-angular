export class License {
  id = 0;
  clientId = 0;
  k1 = '';
  k2 = '';
  start: any;
  end: any;
  type: any;
  position = 0;
  period = 0;

  constructor(data: any) {
    for (const prop in data) {
      if (data.hasOwnProperty(prop)) {
        this[prop] = data[prop];
      }
    }
  }
}
